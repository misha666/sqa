#include <iostream>

#include "Bar.hpp"

int main()
{
	int unused = 5;
	if (constexpr auto number = 5u; true == isBiggerThanTen(number))
	{
        std::cout << number << " is bigger than ten :) \n";
	}
	else
	{
        std::cout << number << " is smallest than ten :) \n";
	}

	return 0;
}
