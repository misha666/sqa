#include <memory>

struct Node
{
    std::shared_ptr<Node> next;
};

int main()
{
    std::shared_ptr<Node> root = std::make_shared<Node>();
    root->next = root;

    return 0;
}
